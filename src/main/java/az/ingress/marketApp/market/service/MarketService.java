package az.ingress.marketApp.market.service;

import az.ingress.marketApp.market.dto.MarketDto;

import java.util.List;

public interface MarketService {
    MarketDto createMarket(MarketDto marketDto);

    MarketDto updateMarket(Integer marketId, MarketDto marketDto);

    List<MarketDto> findAll();

    MarketDto findById(Integer id);
    void deleteMarket(Integer id);


}
