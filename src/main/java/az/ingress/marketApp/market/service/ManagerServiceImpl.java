package az.ingress.marketApp.market.service;


import az.ingress.marketApp.market.dto.ManagerDto;
import az.ingress.marketApp.market.mapper.ManagerMapper;
import az.ingress.marketApp.market.model.Manager;
import az.ingress.marketApp.market.repositoty.ManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final ManagerRepository managerRepository;
    private final ManagerMapper managerMapper;

    @Override
    public ManagerDto createManager(ManagerDto managerDto) {
        Manager manager = managerMapper.mapToManager(managerDto);
        Manager savedManager = managerRepository.save(manager);
        return managerMapper.mapToManagerDto(savedManager);
    }

    @Override
    public ManagerDto updateManager(Integer managerId, ManagerDto managerDto) {
        Manager managerDb = managerRepository.cFindById(managerId).orElseThrow(RuntimeException::new);
        Manager manager = managerMapper.mapToManager(managerDto);
        manager.setId(managerId);
        if (managerDto.getPhones().isEmpty()) {
            manager.setPhones(managerDb.getPhones());
        }
        Manager savedManager = managerRepository.save(manager);
        return managerMapper.mapToManagerDto(savedManager);
    }

    @Override
    public List<ManagerDto> findAll() {
        return managerMapper.mapToManagerDtoList(managerRepository.cFindAll());
    }

    @Override
    public ManagerDto findById(Integer id) {
        Manager managerDb = managerRepository.cFindById(id).orElseThrow(RuntimeException::new);
        return managerMapper.mapToManagerDto(managerDb);    }

    @Override
    public void deleteManager(Integer id) {
        managerRepository.cFindById(id).orElseThrow(RuntimeException::new);
        managerRepository.deleteById(id);
    }
}
