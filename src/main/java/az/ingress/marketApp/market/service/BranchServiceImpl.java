package az.ingress.marketApp.market.service;



import az.ingress.marketApp.market.dto.BranchDto;
import az.ingress.marketApp.market.mapper.BranchMapper;
import az.ingress.marketApp.market.model.Branch;
import az.ingress.marketApp.market.model.Manager;
import az.ingress.marketApp.market.model.Market;
import az.ingress.marketApp.market.repositoty.BranchRepository;
import az.ingress.marketApp.market.repositoty.ManagerRepository;
import az.ingress.marketApp.market.repositoty.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService{
    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final ManagerRepository managerRepository;
    private final BranchMapper branchMapper;


    @Override
    public BranchDto createBranch(Integer marketId, Integer managerId, BranchDto branchDto) {
        Market marketDb = marketRepository.cFindById(marketId).orElseThrow(RuntimeException::new);
        Manager managerDb = managerRepository.cFindById(managerId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.mapToBranch(branchDto);
        branch.setMarket(marketDb);
        branch.setManager(managerDb);
        return branchMapper.mapToBranchDto(branchRepository.save(branch));
    }

    @Override
    public BranchDto updateBranch(Integer branchId, BranchDto branchDto) {
        Branch branchDb = branchRepository.cFindById(branchId).orElseThrow(RuntimeException::new);
        Branch branch =  branchMapper.mapToBranch(branchDto);
        branch.setId(branchId);
        branch.setManager(branchDb.getManager());
        branch.setMarket(branchDb.getMarket());

        if (branchDto.getPhones().isEmpty())
        {
            branch.setPhones(branchDb.getPhones());
        }

        Branch savedBranch = branchRepository.save(branch);
        return branchMapper.mapToBranchDto(savedBranch);
    }

    @Override
    public BranchDto updateManagerForBranch(Integer branchId, Integer managerId) {
        Branch branchDb = branchRepository.cFindById(branchId).orElseThrow(RuntimeException::new);
        Manager managerDb = managerRepository.cFindById(managerId).orElseThrow(RuntimeException::new);
        branchDb.setManager(managerDb);
        return branchMapper.mapToBranchDto(branchRepository.save(branchDb));
    }

    @Override
    public BranchDto updateMarketForBranch(Integer branchId, Integer marketId) {
        Branch branchDb = branchRepository.cFindById(branchId).orElseThrow(RuntimeException::new);
        Market marketDb = marketRepository.cFindById(marketId).orElseThrow(RuntimeException::new);
        branchDb.setMarket(marketDb);
        return branchMapper.mapToBranchDto(branchRepository.save(branchDb));
    }

    @Override
    public List<BranchDto> findAll() {
        return branchMapper.mapToBranchDtoList(branchRepository.cFindAll());
    }

    @Override
    public BranchDto findById(Integer id) {
        Branch branchDb = branchRepository.cFindById(id).orElseThrow(RuntimeException::new);
        return branchMapper.mapToBranchDto(branchDb);
    }

    @Override
    public void deleteBranch(Integer id) {
        branchRepository.cFindById(id).orElseThrow(RuntimeException::new);
        branchRepository.deleteById(id);
    }
}

