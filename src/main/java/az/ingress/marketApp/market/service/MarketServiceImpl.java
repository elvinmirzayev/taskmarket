package az.ingress.marketApp.market.service;



import az.ingress.marketApp.market.dto.MarketDto;
import az.ingress.marketApp.market.mapper.MarketMapper;
import az.ingress.marketApp.market.model.Market;
import az.ingress.marketApp.market.repositoty.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {
    private final MarketRepository marketRepository;
    private final MarketMapper marketMapper;

    @Override
    public MarketDto createMarket(MarketDto marketDto) {
        Market market = marketMapper.mapToMarket(marketDto);
        Market savedMarket = marketRepository.save(market);
        return marketMapper.mapToMarketDto(savedMarket);
    }
    @Override
    public MarketDto updateMarket(Integer marketId, MarketDto marketDto) {
        marketRepository.cFindById(marketId).orElseThrow(RuntimeException::new);
        Market market = marketMapper.mapToMarket(marketDto);
        market.setId(marketId);
        Market savedMarket = marketRepository.save(market);
        return marketMapper.mapToMarketDto(savedMarket);
    }
    @Override
    public List<MarketDto> findAll() {
        return marketMapper.mapToMarketDtoList(marketRepository.cFindByAll());
    }

    @Override
    public MarketDto findById(Integer id) {
        Market marketDb = marketRepository.cFindById(id).orElseThrow(RuntimeException::new);
        return marketMapper.mapToMarketDto(marketDb);
    }

    @Override
    public void deleteMarket(Integer id) {
        marketRepository.cFindById(id).orElseThrow(RuntimeException::new);
        marketRepository.deleteById(id);
    }
}
