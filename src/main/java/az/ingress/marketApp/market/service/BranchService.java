package az.ingress.marketApp.market.service;

import az.ingress.marketApp.market.dto.BranchDto;

import java.util.List;

public interface BranchService {
    BranchDto createBranch(Integer marketId, Integer managerId, BranchDto branchDto);

    BranchDto updateBranch(Integer branchId, BranchDto branchDto);

    BranchDto updateManagerForBranch(Integer branchId, Integer managerId);

    BranchDto updateMarketForBranch(Integer branchId, Integer marketId);

    List<BranchDto> findAll();
    void deleteBranch(Integer id);


    BranchDto findById(Integer id);
}

