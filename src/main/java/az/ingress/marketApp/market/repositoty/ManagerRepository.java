package az.ingress.marketApp.market.repositoty;

import az.ingress.marketApp.market.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ManagerRepository extends JpaRepository<Manager,Integer> {
    @Query(value = "select b from Manager b left join fetch b.phones where b.id=:id")
    Optional<Manager> cFindById(Integer id);

    @Query(value = "select b from Manager b left join fetch b.phones")
    List<Manager> cFindAll();
}
