package az.ingress.marketApp.market.controller;


import az.ingress.marketApp.market.dto.BranchDto;
import az.ingress.marketApp.market.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping("/market/{marketId}/manager/{managerId}/branch")
    public BranchDto createBranch(@PathVariable Integer marketId, @PathVariable Integer managerId, @RequestBody BranchDto branchDto) {
        return branchService.createBranch(marketId, managerId, branchDto);
    }

    @PutMapping("/branch/{id}")
    public BranchDto updateBranch(@PathVariable Integer id, @RequestBody BranchDto branchDto) {
        return branchService.updateBranch(id, branchDto);
    }

    @PutMapping("/manager/{managerId}/branch/{branchId}")
    public BranchDto updateManagerForBranch(@PathVariable Integer branchId, @PathVariable Integer managerId) {
        return branchService.updateManagerForBranch(branchId, managerId);
    }

    @PutMapping("/market/{marketId}/branch/{branchId}")
    public BranchDto updateMarketForBranch(@PathVariable Integer branchId, @PathVariable Integer marketId) {
        return branchService.updateMarketForBranch(branchId, marketId);
    }

    @GetMapping("/branch")
    public List<BranchDto> getBranchAll() {
        return branchService.findAll();
    }

    @GetMapping("/branch/{id}")
    public BranchDto getBranchById(@PathVariable Integer id) {
        return branchService.findById(id);
    }

    @DeleteMapping("/branch/{id}")
    public void deleteBranch(@PathVariable Integer id) {
        branchService.deleteBranch(id);
    }
}

