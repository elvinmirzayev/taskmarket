package az.ingress.marketApp.market.controller;


import az.ingress.marketApp.market.dto.ManagerDto;
import az.ingress.marketApp.market.service.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class ManagerController {

    private final ManagerService managerService;

    @PostMapping("/manager")
    public ManagerDto createManager(@RequestBody ManagerDto managerDto) {
        return managerService.createManager(managerDto);
    }

    @PutMapping("/manager/{id}")
    public ManagerDto updateManager(@PathVariable Integer id, @RequestBody ManagerDto managerDto) {
        return managerService.updateManager(id,managerDto);
    }

    @GetMapping("/manager")
    public List<ManagerDto> getManagerAll() {
        return managerService.findAll();
    }

    @GetMapping("/manager/{id}")
    public ManagerDto getManagerById(@PathVariable Integer id) {
        return managerService.findById(id);
    }

    @DeleteMapping("/manager/{id}")
    public void deleteManager(@PathVariable Integer id) {
        managerService.deleteManager(id);
    }
}



