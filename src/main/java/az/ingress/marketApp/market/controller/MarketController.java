package az.ingress.marketApp.market.controller;


import az.ingress.marketApp.market.dto.MarketDto;
import az.ingress.marketApp.market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
@Valid
public class MarketController {
    private final MarketService marketService;

    @PostMapping("/market")
    public MarketDto createMarket(@RequestBody MarketDto marketDto) {
        return marketService.createMarket(marketDto);
    }

    @PutMapping("/market/{id}")
    public MarketDto updateMarket(@PathVariable Integer id, @RequestBody MarketDto marketDto) {
        return marketService.updateMarket(id,marketDto);
    }

    @GetMapping("/market")
    public List<MarketDto> getMarketAll() {
        return marketService.findAll();
    }

    @GetMapping("/market/{id}")
    public MarketDto getMarketById(@PathVariable Integer id) {
        return marketService.findById(id);
    }

    @DeleteMapping("/market/{id}")
    public void deleteMarket(@PathVariable Integer id) {
        marketService.deleteMarket(id);
    }
}

