package az.ingress.marketApp.market.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ManagerDto {
    @NotBlank
    String name;
    @NotBlank
    String surname;
    @NotBlank
    Integer age;
    @Valid
    List<PhoneDto> phones;

}


